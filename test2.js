var assert = require('assert');
var webdriver = require('selenium-webdriver'),
   By = webdriver.By,
   until = webdriver.until;

var chrome = require('chromedriver');

var driver = new webdriver.Builder()
             .forBrowser('chrome')
             .setChromeOptions('--headless','disable-popup-blocking','test-type','--no-sandbox')
             .build();

//Log in in the page
driver.get('http://jazzsports.ag/');
driver.findElement(By.name('account')).sendKeys('test73');
driver.findElement(By.name('password')).sendKeys('pph73');
driver.findElement(By.xpath("//input[@value='login']")).submit();
//Select in the menu the casino platinum option
driver.sleep(10000);
driver.findElement(By.xpath('/html/body/form/div/div[1]/div[3]/div[4]/a/img[@src="https://media.betimages.com/media/banner/ONLINE CASINO.jpg"]')).click();
// Go to the frame that contains the casino platinum
driver.switchTo().frame(1);
//interaction with elements of the casino platium to check if it is working
driver.sleep(9000);
driver.findElement(By.id('a.featured')).click();
driver.findElement(By.id('a.table_games')).click();
driver.findElement(By.id('a.video_pokers')).click();
driver.findElement(By.id('a.slot_games')).click();
driver.findElement(By.id('a.zippedkeno')).click();
//Go back to main frame
driver.switchTo().defaultContent();
//Close
driver.quit();
